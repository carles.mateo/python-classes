

if __name__ == "__main__":

    s_text = """Python is an interpreted high-level general-purpose programming language. Its design philosophy emphasizes code readability with its use of significant indentation. Its language constructs as well as its object-oriented approach aim to help programmers write clear, logical code for small and large-scale projects.[30]
Python is dynamically-typed and garbage-collected. It supports multiple programming paradigms, including structured (particularly, procedural), object-oriented and functional programming. It is often described as a "batteries included" language due to its comprehensive standard library.[31]
Guido van Rossum began working on Python in the late 1980s, as a successor to the ABC programming language, and first released it in 1991 as Python 0.9.0.[32] Python 2.0 was released in 2000 and introduced new features, such as list comprehensions and a garbage collection system using reference counting. Python 3.0 was released in 2008 and was a major revision of the language that is not completely backward-compatible. Python 2 was discontinued with version 2.7.18 in 2020.[33]
Python consistently ranks as one of the most popular programming languages.[34][35][36][37]
Source: https://en.wikipedia.org/wiki/Python_(programming_language)"""

    d_counter = {}

    s_char_max = ""
    i_times_max = 0

    for s_char in s_text:
        if s_char == " ":
            continue

        i_times = d_counter.get(s_char)
        if i_times is None:
            i_times = 0
        i_times = i_times + 1
        d_counter[s_char] = i_times

        if i_times > i_times_max:
            i_times_max = i_times
            s_char_max = s_char

    print("The char more repeated is", s_char_max, i_times_max, "times")
    # If you want to see the complete dictionary use:
    # print(d_counter)
