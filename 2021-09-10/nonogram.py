#!/usr/bin/env python
"""
Source:
https://www.reddit.com/r/dailyprogrammer/comments/o4uyzl/20210621_challenge_395_easy_nonogram_row/

Challenge:
This challenge is inspired by nonogram puzzles, but you don't need to be familiar
with these puzzles in order to complete the challenge.
(https://en.wikipedia.org/wiki/Nonogram#Example)

A binary array is an array consisting of only the values 0 and 1. Given a binary
array of any length, return an array of positive integers that represent the lengths
of the sets of consecutive 1's in the input array, in order from left to right.

nonogramrow([]) => []
nonogramrow([0,0,0,0,0]) => []
nonogramrow([1,1,1,1,1]) => [5]
nonogramrow([0,1,1,1,1,1,0,1,1,1,1]) => [5,4]
nonogramrow([1,1,0,1,0,0,1,1,1,0,0]) => [2,1,3]
nonogramrow([0,0,0,0,1,1,0,0,1,0,1,1,1]) => [2,1,3]
nonogramrow([1,0,1,0,1,0,1,0,1,0,1,0,1,0,1]) => [1,1,1,1,1,1,1,1]

As a special case, nonogram puzzles usually represent the empty output ([]) as [0].
If you prefer to do it this way, that's fine, but 0 should not appear in the output
in any other case.
"""

# Solution by Carles Mateo


def nonogramrow(a_numbers):
    a_result = []

    i_sum = 0
    for i_num in a_numbers:
        if i_num == 0:
            if i_sum > 0:
                a_result.append(i_sum)
                i_sum = 0
        else:
            i_sum += 1

    # The last row
    if i_sum > 0:
        a_result.append(i_sum)

    return a_result


if __name__ == "__main__":
    assert nonogramrow([0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1]) == [2, 1, 3], "Incorrect result, please try again."
    assert nonogramrow([]) == [], "Incorrect result, please try again."
    assert nonogramrow([0,0,0,0,0]) == [], "Incorrect result, please try again."
    assert nonogramrow([1,1,1,1,1]) == [5], "Incorrect result, please try again."
    assert nonogramrow([0,1,1,1,1,1,0,1,1,1,1]) == [5,4], "Incorrect result, please try again."
    assert nonogramrow([1,1,0,1,0,0,1,1,1,0,0]) == [2,1,3], "Incorrect result, please try again."
    assert nonogramrow([0,0,0,0,1,1,0,0,1,0,1,1,1]) == [2,1,3], "Incorrect result, please try again."
    assert nonogramrow([1,0,1,0,1,0,1,0,1,0,1,0,1,0,1]) == [1,1,1,1,1,1,1,1], "Incorrect result, please try again."
    print("Success!")


