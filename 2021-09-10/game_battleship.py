# Author: Carles Mateo

import random


class PlayerMap:

    def __init__(self, i_width=20, i_height=20, b_debug=False):
        self.i_width = i_width
        self.i_height = i_height
        self.player_map = []
        self.generate_new_empty_map()

        self.i_size_destroyer = 7
        self.i_size_big = 5
        self.i_size_medium = 4
        self.i_size_little = 3

        # Total cells occupied by ships
        self.i_total_cells_of_ships = 0
        self.i_hits_by_enemy = 0

        self.b_debug = b_debug

        self.i_total_space = i_width * i_height

        self.h_size_ships = {"destroyer": {"length": 7,
                                           "number": 1},
                             "big": {"length": 5,
                                     "number": 2},
                             "medium": {"length": 4,
                                        "number": 3},
                             "little": {"length": 3,
                                        "number": 5},

                             }

    def d(self, s_text):
        """
        Prints any text if b_debug variable is set to true
        :param s_text:
        :return:
        """
        if self.b_debug is True:
            print("Debug: " + s_text)

    def get_i_width(self):
        return self.i_width

    def get_i_height(self):
        return self.i_height

    def generate_new_empty_map(self):
        """
        Generate a map with spaces for sea
        :return:
        """

        self.i_total_cells_of_ships = 0
        self.i_hits_by_enemy = 0

        for i_row in range(0, self.get_i_height()):
            s_row = " " * self.i_width
            self.player_map.append(s_row)

    def get_map_header(self):
        """
        Get the X coordinates
        :return:
        """
        s_map_header = ""
        s_map_header = s_map_header + "   "
        for i_index in range(65, 65 + self.get_i_width()):
            s_map_header = s_map_header + " " + chr(i_index)

        return s_map_header


    def get_map(self):
        s_map = ""
        s_map = s_map + self.get_map_header()
        s_map = s_map + "\n"
        s_map = s_map + "   " + "_" * ((self.i_width * 2) + 1) + "\n"
        i_row_number = 0
        for s_row in self.player_map:
            i_row_number = i_row_number + 1
            s_row_number = str(i_row_number).rjust(2) + " "
            s_map = s_map + s_row_number + "|"
            for s_char in s_row:
                s_map = s_map + s_char + "|"
            s_map = s_map + "\n"
        s_map = s_map + "   " + "-" * ((self.i_width * 2) + 1) + "\n"

        return s_map

    def get_map_from_opponent_point_of_view(self):
        """
        Hide the ships, shows the shoots
        :return:
        """
        s_map = ""
        s_map = s_map + self.get_map_header()
        s_map = s_map + "\n"
        s_map = s_map + "   " + "_" * ((self.i_width * 2) + 1) + "\n"
        i_row_number = 0
        for s_row in self.player_map:
            i_row_number = i_row_number + 1
            s_row_number = str(i_row_number).rjust(2) + " "
            s_map = s_map + s_row_number + "|"
            for s_char in s_row:
                if s_char in " *#123456789":
                    # Make data anonymous
                    s_char = "?"
                s_map = s_map + s_char + "|"
            s_map = s_map + "\n"
        s_map = s_map + "   " + "-" * ((self.i_width * 2) + 1) + "\n"

        return s_map

    def assign_ship_randomly(self, s_type):
        # Get a coordinate Randomly
        i_x = random.randrange(self.i_width)
        i_y = random.randrange(self.i_height)
        # 0 Vertical 1 Horizontal
        i_direction = random.randrange(2)

        i_length = self.h_size_ships[s_type]["length"]

        if i_direction == 0:
            # Vertical
            return self.check_if_ship_can_be_allocated_vertically(i_x, i_y, i_length)
        else:
            # Horizontal
            return self.check_if_ship_can_be_allocated_horizontally(i_x, i_y, i_length)

    def replace_row_for_vertical_ship(self, s_row, i_x, b_top_or_bottom=False, s_char_ship="#"):
        s_row_new = ""
        for i_index in range(0, self.i_width):
            if i_index == i_x - 1:
                s_row_new = s_row_new + "*"
            elif i_index == i_x:
                if b_top_or_bottom is False:
                    s_row_new = s_row_new + s_char_ship
                else:
                    s_row_new = s_row_new + "*"
            elif i_index == i_x + 1:
                s_row_new = s_row_new + "*"
            else:
                s_row_new = s_row_new + s_row[i_index]

        return s_row_new

    def replace_column_for_horizontal_ship(self, a_player_map_copy, i_index_x, i_y, b_top_or_bottom, s_char_ship="#"):
        for i_index_y in range(i_y - 1, i_y + 2):
            s_row_new = ""
            if i_index_y < 0 or i_index_y >= self.i_height:
                # Don't print anything, is out of the borders
                continue

            s_row_new = ""
            self.d("char=" + s_char_ship + " y=" + str(i_index_y) + " row=" + a_player_map_copy[i_index_y])
            for i_index_horizontal in range(0, self.i_width):
                s_char = a_player_map_copy[i_index_y][i_index_horizontal]
                if i_index_horizontal == i_index_x:
                    # Do only for the i_x, the rest copy
                    if i_index_y == i_y - 1:
                        s_char = "*"
                    elif i_index_y == i_y + 1:
                        s_char = "*"
                    elif i_index_y == i_y:
                        if b_top_or_bottom is True:
                            s_char = "*"
                        else:
                            s_char = s_char_ship

                s_row_new = s_row_new + s_char

            a_player_map_copy[i_index_y] = s_row_new
            self.d("After: y=" + str(i_index_y) +  " row=" + a_player_map_copy[i_index_y])

    def check_if_ship_can_be_allocated_vertically(self, i_x, i_y, i_length):
        # To optimizing, we are going to modify the map on the fly.
        # In the case we have to rollback, we only update the real map if all was good.
        a_player_map_copy = self.player_map.copy()

        if i_y + i_length <= self.i_height:
            # Ok, check if any collision
            # Check up position, as must be sea
            i_index_y = i_y - 1
            if i_index_y < 0:
                i_index_y = 0

            while i_index_y <= i_y + i_length:
                if i_index_y == self.i_height:
                    break

                b_update_row = False
                b_top_or_bottom = False
                self.d("Attempting vertical x=" + str(i_x) + " y=" + str(i_index_y))
                if i_index_y == i_y - 1 or i_index_y == i_y + i_length:
                    b_top_or_bottom = True
                # Note: We check for other ships or their borders
                if i_x > 0:
                    s_char = a_player_map_copy[i_index_y][i_x - 1]
                    if s_char != " ":
                        return False
                    b_update_row = True

                s_char = a_player_map_copy[i_index_y][i_x]
                if s_char != " ":
                    return False
                b_update_row = True

                if i_x < self.i_width - 1:
                    s_char = a_player_map_copy[i_index_y][i_x + 1]
                    if s_char != " ":
                        return False
                    b_update_row = True

                if b_update_row is True:
                    s_row_updated = self.replace_row_for_vertical_ship(a_player_map_copy[i_index_y], i_x, b_top_or_bottom, s_char_ship=str(i_length)[0])
                    a_player_map_copy[i_index_y] = s_row_updated

                i_index_y += 1

            # Validations went well
            self.player_map = a_player_map_copy.copy()

            return True

        else:
            return False

    def check_if_ship_can_be_allocated_horizontally(self, i_x, i_y, i_length):
        # To optimizing, we are going to modify the map on the fly.
        # In the case we have to rollback, we only update the real map if all was good.
        a_player_map_copy = self.player_map.copy()

        if i_x + i_length <= self.i_width:
            # Ok, check if any collision
            # Check up position, as must be sea
            i_index_x = i_x - 1
            if i_index_x < 0:
                i_index_x = 0

            while i_index_x <= i_x + i_length:
                # Note: We check for other ships or their borders
                if i_index_x == self.i_width:
                    break

                b_update_row = False
                b_top_or_bottom = False

                self.d("Attempting horizontal x=" + str(i_index_x) + " y=" + str(i_y))
                if i_index_x == i_x - 1 or i_index_x == i_x + i_length:
                    b_top_or_bottom = True
                # Note: We check for other ships or their borders
                if i_y > 0:
                    s_char = a_player_map_copy[i_y - 1][i_index_x]
                    if s_char != " ":
                        return False
                    b_update_row = True

                s_char = a_player_map_copy[i_y][i_index_x]
                if s_char != " ":
                    return False
                b_update_row = True

                if i_y < self.i_height - 1:
                    s_char = a_player_map_copy[i_y + 1][i_index_x]
                    if s_char != " " and s_char != "*":
                        return False
                    b_update_row = True

                if b_update_row is True:
                    self.replace_column_for_horizontal_ship(a_player_map_copy, i_index_x, i_y, b_top_or_bottom,
                                                                       s_char_ship=str(i_length)[0])

                i_index_x += 1

            # Validations went well
            self.player_map = a_player_map_copy.copy()

            return True

        else:
            return False

    def allocate_ships_randomly(self):
        for s_ship_type in self.h_size_ships:
            self.d("Allocating: " + s_ship_type)
            i_ships_pending_allocation = self.h_size_ships[s_ship_type]["number"]
            self.d("Ships to deploy: " + str(i_ships_pending_allocation))
            while i_ships_pending_allocation > 0:
                i_max_attempts = 1000
                i_attempt = 0
                b_allocated = False
                while i_attempt < i_max_attempts:
                    i_attempt += 1
                    b_allocated = self.assign_ship_randomly(s_ship_type)
                    if b_allocated is True:
                        i_ships_pending_allocation = i_ships_pending_allocation - 1
                        self.i_total_cells_of_ships = self.i_total_cells_of_ships + self.h_size_ships[s_ship_type]["length"]
                        self.d("Ship " + s_ship_type + " allocated")
                        break

                if b_allocated is False:
                    print("Sorry, I was unable to allocate the ship in the map.")
                    print(self.get_map())
                    exit(1)

    def check_incoming_shoot(self, i_x, i_y):
        """
        Check if an incoming shoot hit any ship
        """
        b_hit = False
        s_row_new = ""
        s_row = self.player_map[i_y]
        s_char = s_row[i_x]

        if s_char == " " or s_char == "*" or s_char == "." or s_char == "X":
            # Failed
            b_hit = False
            s_new_char = "."
        else:
            # Hit
            b_hit = True
            s_new_char = "X"
            self.i_hits_by_enemy = self.i_hits_by_enemy + 1

        # Replace the position
        i_index = 0
        for s_char_index in s_row:
            if i_index == i_x:
                s_char_index = s_new_char
            s_row_new = s_row_new + s_char_index
            i_index += 1

        self.player_map[i_y] = s_row_new

        return b_hit

    def gen_a_shoot_from_computer(self):
        """
        Generate a shoot from the computer.
        If there is an already hit ship, we should focus on that.
        Otherwise, shoot randomly.
        :return:
        """
        i_x = random.randrange(self.i_width)
        i_y = random.randrange(self.i_height)

        b_hit = False
        s_row_new = ""
        s_row = self.player_map[i_y]
        s_char = s_row[i_x]

        if s_char == " " or s_char == "*" or s_char == "." or s_char == "X":
            # Failed
            b_hit = False
            s_new_char = "."
        else:
            # Hit
            b_hit = True
            s_new_char = "X"
            self.i_hits_by_enemy = self.i_hits_by_enemy + 1

        # Replace the position
        i_index = 0
        for s_char_index in s_row:
            if i_index == i_x:
                s_char_index = s_new_char
            s_row_new = s_row_new + s_char_index
            i_index += 1

        self.player_map[i_y] = s_row_new

        return b_hit, i_x, i_y

    def get_enemy_won(self):
        """
        Checks if the enemy won
        :return: bool
        """

        if self.i_hits_by_enemy >= self.i_total_cells_of_ships:
            return True

        return False

def get_coordinates(s_text, o_map):
    """
    Return the coordinates relative to the map (starting with 0)
    :param s_text:
    :return: b_correct, i_column, i_row
    """

    if len(s_text) < 2:
        return False

    # We assume our map will always be one single
    s_column = s_text[0]
    i_column = ord(s_column)
    # Subtract the base ASCii character to get the index
    i_column = i_column - 65
    if i_column < 0 or i_column >= o_map.i_width:
        return False, 0, 0

    s_row = s_text[1:]
    try:
        i_row = int(s_row)
    except:
        return False, 0, 0

    if i_row < 0 or i_row > o_map.i_height:
        return False, 0, 0

    # All good, subtract 1 for the Array
    i_row = i_row - 1

    return True, i_column, i_row


if __name__ == "__main__":
    o_player_map = PlayerMap(20, 20)
    o_computer_map = PlayerMap(20, 20)

    o_player_map.allocate_ships_randomly()
    o_computer_map.allocate_ships_randomly()
    print("Player's map:")
    print(o_player_map.get_map())

    print()
    print("Computer's map:")
    print(o_computer_map.get_map_from_opponent_point_of_view())

    i_turn = 0

    while True:
        i_turn += 1

        print()
        print("Turn", i_turn)
        print()
        s_coords = input("Choose your coordinates to fire (i.e.: A1) :")
        b_good_coords, i_x, i_y = get_coordinates(s_coords, o_computer_map)
        if b_good_coords is False:
            print("Wrong coordinates")
            continue

        b_hit = o_computer_map.check_incoming_shoot(i_x, i_y)
        if b_hit is True:
            print("You hit a ship!")
            b_you_win = o_computer_map.get_enemy_won()
            if b_you_win is True:
                print("You win!")
                break
        else:
            print("Water!")

        # Turn of the computer
        b_hit_by_computer, i_x, i_y = o_player_map.gen_a_shoot_from_computer()
        print("The computer fired on " + chr(65 + i_x + 1) + str(i_y + 1))
        if b_hit_by_computer is True:
            print("The computer hit you!")
            b_computer_wins = o_player_map.get_enemy_won()
            if b_computer_wins is True:
                print("The computer wins!")
                break
        else:
            print("Water!")

        # Print maps
        print("Player's map:")
        print(o_player_map.get_map())

        print()
        print("Computer's map:")
        print(o_computer_map.get_map_from_opponent_point_of_view())

        print()
        print("Ships occupying cells: " + str(o_computer_map.i_total_cells_of_ships) + " Cells destroyed: " + str(o_computer_map.i_hits_by_enemy))




# Decided to add getters