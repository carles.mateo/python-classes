
def first():
    print("I'm first")
    second()

first()

def second():
    print("I'm second")


if __name__ == "__main__":
    print("How important is to execute only after Python has loaded the code")
