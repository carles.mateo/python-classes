def average(a_numbers):
    i_count = len(a_numbers)
    if i_count == 0:
        return 0
    i_total = 0
    for i_num in a_numbers:
        i_total += i_num

    f_average = i_total/i_count

    return f_average


if __name__ == "__main__":
    a_values = [7, 5, 3, 21, 30.5, 14, 8, 72, 9.9, 42]
    f_average = average(a_values)
    print("The average is", f_average)
