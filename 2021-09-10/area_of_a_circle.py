import math


def area_circle(f_radius):
    f_area = math.pi * f_radius**2

    return f_area


if __name__ == "__main__":
    f_area = area_circle(5)
    print(f_area)
