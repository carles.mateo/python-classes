

a_total_temperatures = []

while True:
    s_answer = input("Please enter the temperature or type exit: ")
    if s_answer == "":
        continue
    if s_answer == "exit":
        break

    f_temperature = float(s_answer)
    a_total_temperatures.append(f_temperature)


if len(a_total_temperatures) > 0:
    print("Total temperatures read:", len(a_total_temperatures))
    print("The minimum temperature is:", min(a_total_temperatures))
    print("The maximum temperature is:", max(a_total_temperatures))
    f_average = sum(a_total_temperatures)/len(a_total_temperatures)
    print("The average temperature is:", f_average)
