
i_counter = 0
f_temp_min = 10000
f_temp_max = -10000
f_total = 0

while True:
    s_answer = input("Please enter the temperature or type exit: ")
    if s_answer == "":
        continue
    if s_answer == "exit":
        break

    f_temperature = float(s_answer)
    if f_temperature < f_temp_min:
        f_temp_min = f_temperature

    if f_temperature > f_temp_max:
        f_temp_max = f_temperature

    f_total = f_total + f_temperature

    i_counter = i_counter + 1

if i_counter > 0:

    f_temp_average = f_total / i_counter

    print("Total temperatures read:", i_counter)
    print("The minimum temperature is:", f_temp_min)
    print("The maximum temperature is:", f_temp_max)
    print("The average temperature is:", f_temp_average)
