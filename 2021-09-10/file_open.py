
if __name__ == "__main__":

    s_filename = "example.txt"

    try:

        o_file = open(s_filename, 'r')
        i_count = 0

        while True:
            # Get the next line from the file
            s_line = o_file.readline()

            # if nothing was read EOF (End of File was reached)
            if not s_line:
                break

            i_count += 1

            # We avoid print adding the \n by removing it from the right w rstrip()
            print("Line {}: {}".format(i_count, s_line.rstrip()))

        o_file.close()
    except FileNotFoundError:
        # An error has occurred, probably the file does not exist
        print("Error accessing the file", s_filename)
        exit(1)

    print("The file has", i_count, "lines")
