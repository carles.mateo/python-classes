
if __name__ == "__main__":
    a_phone_pref = [("Ireland", "+353"), ("Catalonia", "+34"), ("US", "+1")]

    for t_prefixes in a_phone_pref:
        s_place, s_prefix = t_prefixes
        print("For", s_place, "the phone prefix is", s_prefix)
