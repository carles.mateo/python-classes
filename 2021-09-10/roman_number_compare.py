#!/usr/env/python3
"""
Source:
https://www.reddit.com/r/dailyprogrammer/comments/oe9qnb/20210705_challenge_397_easy_roman_numeral/

Challenge:
For the purpose of today's challenge, a Roman numeral is a non-empty
string of the characters M, D, C, L, X, V, and I, each of which has the
value 1000, 500, 100, 50, 10, 5, and 1. The characters are arranged in
descending order, and the total value of the numeral is the sum of the
values of its characters.

For example, the numeral MDCCXXVIIII has the value:
1000 + 500 + 2x100 + 2x10 + 5 + 4x1 = 1729.

This challenge uses only additive notation for roman numerals. There's
also subtractive notation, where 9 would be written as IX. You don't
need to handle subtractive notation (but you can if you want to, as an
optional bonus).

Given two Roman numerals, return whether the first one is less than the
second one:

numcompare("I", "I") => false
numcompare("I", "II") => true
numcompare("II", "I") => false
numcompare("V", "IIII") => false
numcompare("MDCLXV", "MDCLXVI") => true
numcompare("MM", "MDCCCCLXXXXVIIII") => false

You only need to correctly handle the case where there are at most 1
each of D, L, and V, and at most 4 each of C, X, and I. You don't need to
validate the input, but you can if you want. Any behavior for invalid
inputs like numcompare("V", "IIIIIIIIII") is fine - true, false, or error.

Try to complete the challenge without actually determining the numerical
values of the inputs.
"""


def numcompare(s_numeral_one, s_numeral_two):
   """
   Return true if numeral_two is bigger than one.
   Do not use numerical values.
   :param s_numeral_one:
   :param s_numeral_two:
   :return: bool
   """

   a_greater = ["I", "V", "X", "L", "C", "D", "M"]
   i_len_min = 0

   i_len1 = len(s_numeral_one)
   i_len2 = len(s_numeral_two)

   # We don't want an overflow
   if i_len1 >= i_len2:
       i_len_min = i_len2
   else:
       i_len_min = i_len1

   for i_index in range(i_len_min):
       s_char1 = s_numeral_one[i_index]
       s_char2 = s_numeral_two[i_index]

       i_pos1 = a_greater.index(s_char1)
       i_pos2 = a_greater.index(s_char2)

       if i_pos2 > i_pos1:
           # We won
           return True
       elif i_pos1 > i_pos2:
           # No need to parse more, s_numeral_one is bigger
           return False

   # So far the Strings are equal, now they can be the same, or one can be different
   if i_len2 > i_len1:
       return True
   elif i_len2 <= i_len1:
       return False


if __name__ == "__main__":
   # Sample test
   assert numcompare("MDCLXV", "MDCLXVI"), "Incorrect result, please try again."
   assert not numcompare("I", "I"), "Incorrect result, please try again."
   assert numcompare("I", "II"), "Incorrect result, please try again."
   assert not numcompare("II", "I"), "Incorrect result, please try again."
   assert not numcompare("V", "IIII"), "Incorrect result, please try again."
   assert not numcompare("MM", "MDCCCCLXXXXVIIII"), "Incorrect result, please try again."

   print("Success!")