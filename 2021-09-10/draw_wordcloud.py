
#from PIL import Image
import matplotlib.pyplot as plt
from wordcloud import WordCloud, ImageColorGenerator

if __name__ == "__main__":

    s_text = """Performance of several languages	171,232	2014-10-13 04:26:37	Web development, Virtualization, Performance, Cloud providers, Software development	Performance, PHP, C, Java, Python, Symfony, Assembler, Perl, Go, Ruby, Bash, JIT Compilers, Luajit, Pypy, Cliff Click, HHVM - Facebook's Hip Hop Virtual Machine, Lua, objdump, Assembler AT&T syntax, Assembler Intel syntax, node.js, Gambas, Phantomjs, PHP 7, PHP 5.6, PHP 5.5, PHP 5.4
    Stopping and investigating a WordPress xmlrpc.php attack	142,929	2014-08-30 22:21:13	Virtualization, Performance, Troubleshoot, Security, Databases, MySQL, CMS, WordPress	Amazon EC2, PHP, DoS, WordPress, xmlrpc, Spam, Apache, MySQL, Plugins WordPress, Slow queries
    Stopping a BitTorrent DDoS attack	63,629	2015-01-23 21:55:35	Performance, Troubleshoot	GET, EzPublish, DoS, WordPress, Symfony, DDoS, SYN Flood, Pirates, Amazon c4.8xlarge, Laravel Framework, Magento, Drupal, Joomla, iptables, Firewall, BitTorrent
    Using Windows 10 Appliance in Ubuntu Virtual Box 4.3.10	30,904	2015-08-23 06:42:44	Web development, Virtualization, Troubleshoot, Casual tech	Windows 10, Microsoft, Ubuntu 14.04, Virtual Box, Bless Hex Editor
    Stopping definitively the massive Distributed DoS attack	24,445	2015-02-01 12:27:16	Bandwidth, Performance, Troubleshoot, Operations	DoS, DDoS, iptables
    Improving performance in PHP	22,914	2014-08-11 02:02:42	Operation costs, Web development, Virtualization, Performance, Cloud providers, NoSql, Software development	Performance, Cassandra, PHP, C, C extensions, RAM, Ramdisk, Database Sharding
    Creating a Content Filter for Postfix in PHP	22,167	2016-06-30 12:43:19	Software development, DevOps	PHP, postfix, content filters, SMTP
    Java validation Classes for Keyboard	21,781	2020-12-15 23:30:49	Software development, Java	Java, java.util.Scanner, Data Validation
    NAS and Gigabit	21,004	2013-08-18 14:54:43	Amazon EC2, Bandwidth, Data Centers, Hardware, NAS, Rack Servers, Virtualization, Performance, Cloud providers	Amazon EC2, Gigabit, NAS, Common Errors, Rack Servers, Blade Servers, Performance, Unix command: dd, disks: SATA, disks: SAS, disks: SSD, Mellanox
    Scaling phantomjs with PHP	19,772	2015-06-17 15:55:04	Bandwidth, Web development, Troubleshoot	Phantomjs, nodejs, SSL, Advisor, ghost-town
    CSort multithread versus QuickSort with Java Source Code	17,661	2017-03-26 19:40:14	Performance, Software development, Algorithms	Java, csort, Quicksort, MultiThreading
    csort, my algorithm that heavily beats quicksort	12,682	2015-05-25 23:09:56	Performance, Software development	PHP, C, Java, JIT Compilers, HHVM - Facebook's Hip Hop Virtual Machine, Algorithms, csort, Big-O, Big Data, Quicksort, Arrays
    Raspberry Pi and osmc	11,727	2015-04-06 11:58:32	Hardware, Casual tech	ssh, RaspBerry Pi, osmc, Debian, htop, Start up
    Begin developing with Cassandra	10,782	2014-07-31 21:45:29	Operation costs, Web development, Performance, NoSql, Software development	NoSql, Cassandra, PHP, LunaCloud, Java, Python
    Linux command-line tools I usually install	10,147	2016-03-11 14:06:31	Troubleshoot, Operations	Performance, htop, mytop, ncdu, Systems, ethtool, iftop, perf, zram
    What is PrototypeC and how you can DIY	10,039	2015-05-08 18:31:36	Hardware, Raspberry Pi 2	RaspBerry Pi, DIY, PrototypeC, Battery, touch screen, Raspberry Pi 2, Wearable, LXDE
    Install Windows Subsystem for Linux, WSL 2 on Windows 10 64 bit, with Ubuntu, solution to error WslRegisterDistribution failed with error: 0x80070057	9,809	2021-01-13 21:48:13	Virtualization, Operating Systems, Windows 10 Pro	Map-Reduce, Debian, htop, Windows 10, Linux, CTOP.py, ETL, WSL - Windows Subsystem for Linux, GitBash, Powershell, Kali Linux
    Why you should not use t1.micro Amazon Free Tier	9,153	2013-08-30 10:31:47	Amazon EC2, Performance, Cloud providers	Amazon EC2, Performance, CMIPS, Vmware
    Troubleshooting upgrading and loading a ZFS module in RHEL7.4	9,017	2018-07-03 02:45:23	Troubleshoot, Software development, DevOps	strace, ZFS, RHEL
    The Cloud is for Scaling	8,490	2013-09-21 22:16:42	Amazon EC2, Bandwidth, Operation costs, Data Centers, NAS, Virtualization, Performance, DigitalOcean	Amazon EC2, Cloud providers, Cost saving, NAS, ISP - Internet Service Provider, CloudWatch, DigitalOcean, SSD, Dropbox
    Things I hate from PHP	8,098	2014-03-30 13:10:51	Web development, Troubleshoot, Software development	PHP, Sermepa, bug, featured
    A simple sample to print colors in Terminal with Python (local tty or stty in a ssh)	6,882	2018-05-17 20:24:07	Casual tech, Python	Python, Python 2.7, Python 3.6
    Upgrade your Scalability with NoSql	6,760	2014-02-15 21:42:48	NoSql	NoSql, Cassandra, MongoDb, Hadoop, Memcached, Redis, Map-Reduce, Riak, Project Voldemort
    Curiosity Python string.strip() removes just more than white spaces	6,500	2018-11-27 19:04:18	Troubleshoot, Software development	Python, Python 2.7, Python 3.6, Curiosity
    A quick note about symmetric encryption and entropy	6,384	2015-04-02 21:53:53	Operation costs, Performance, Software development	Java, UUID, encryption, Security, c-client
    ZFS Improving iSCSI performance for Block Devices (trick for Volumes)	6,359	2018-10-19 11:18:53	NAS, Performance, Storage	ZFS, zvol, iSCSI
    CTOP.py	6,089	2020-01-12 17:39:36	Data Centers, Virtualization, Cloud providers, Troubleshoot, Operations, Docker Containers	Firewall, htop, iftop, top, ctop
    An Epic fail that are committing all the universities	5,397	2018-06-14 19:35:13	Performance, Troubleshoot, Software development, Hiring	C, Python, Philosophy, University, Code
    Some advice for WFH	5,355	2020-05-17 18:10:12	Casual tech, Workplace	Remote Work, WFH - Work from Home
    My talk at OpenZFS 2018 about DRAID	5,063	2018-09-15 02:28:54	Software development, Travel and countries, Storage	ZFS, DRAID, OpenZFS
    Adding my Server as Docker, with PHP Catalonia Framework, explained	4,889	2019-07-03 07:00:43	Web development, Virtualization, Cloud providers, DevOps, Docker Containers	Amazon EC2, Apache, Catalonia Framework, IaC Infrastructure as Code, git, lynx, Docker, Dockerfile
    Creating a compressed filesystem with Linux and ZFS (using just files)	4,601	2018-09-26 15:48:56	Performance, Storage	ZFS, Compression, LZ4
    Adding a RAMDISK as SLOG ZIL to ZFS	4,397	2020-08-23 10:51:46	Storage, MDRAID, NFS, ZFS	SSD, Ramdisk, ZFS, SSD drives, ZIL, NVMe, IOC, SLOG, SSD Enterprise, 10GbE, Bonding, zpool, JBOD
    A sample forensic post mortem for a iSCSI Initiator (client) that had connectivity problems to the Server	4,326	2019-08-12 18:30:54	Troubleshoot, Storage	ZFS, iSCSI, Linux, Post-Mortem, Forensic
    Solving Oracle error ORA 600 [KGL-heap-size-exceeded]	4,268	2021-02-01 19:38:37	Web development, Troubleshoot, SRE, Oracle	Oracle 10g, Oracle, Error ORA 600, KGL-heap-size-exceeded
    Why Cloud is not optional	4,168	2013-02-22 23:12:38	Amazon EC2, Bandwidth, Operation costs, Cloud providers	Amazon EC2, Cloud providers, Servers, Instances, Cost saving
    Dealing with Performance degradation on ZFS (DRAID) Rebuilds when migrating from a single processor to a multiprocessor platform	4,094	2019-06-07 14:49:30	Data Centers, Hardware, NAS, Rack Servers, Performance, Troubleshoot, Operations, Storage	CMIPS, htop, ZFS, DRAID, SAS, Multiprocessor, Memory, dmidecode, Expanders, iostat, dd, sg utils, lscpu, 4U90, 4U60, 4602, Spinning drives, SSD drives, QPI
    Resources for Microservices and Business Domain Solutions for the Cloud Architect / Microservices Architect	3,911	2019-10-05 21:32:45	Operation costs, Web development, Performance, Software development, Docker Containers, Microservices, Python	NoSql, PHP, Java, Python, Docker, Microservices, Spring Cloud, Spring Boot, Kubernetes, Business Domain, CI/CD, Galileo, Amadeus, GDS, ASP.NET Core, Flask
    Create a small partition on the drives for tests	3,852	2019-04-18 16:46:39	NAS, Troubleshoot, Operations, Storage	Bash, ZFS, fdisk, partition
    Working in Cork, Ireland, for IT Engineers	3,837	2020-02-23 20:22:33	Travel and countries, Ireland	Cork
    Some handy tricks for working with ZFS	3,831	2019-06-25 21:50:06	NAS, Storage	Ramdisk, ZFS, DRAID, partition, dd, parted, gpt, MBR, ZIL, screen
    Solving a persistent MD Array problem in RHEL7.4 (Dual Port SAS drives)	3,797	2018-10-31 20:02:57	Hardware, Troubleshoot, DevOps, Storage, MDRAID	ZFS, RHEL, DRAID, MDRAID, RAID6, Dual port SAS
    Lesson 0, learning to code in Python for non programmers	3,740	2020-03-31 18:47:02	Software development, Casual tech, Python	Python, PyCharm, Video
    Simulating a SAS physical pull out of a drive	3,718	2019-03-14 14:13:14	Hardware, Operations, Storage	ZFS, SAS, Testing
    Working in Barcelona, Catalonia, for IT Engineers	3,691	2020-02-23 21:20:14	Travel and countries, Catalonia	Barcelona
    A script to backup your partition compressed and a game to learn about dd and pipes	3,660	2020-05-24 18:20:00	Operations, Storage, Bash"""

    o_word_cloud = WordCloud(height=800,
                             width=800,
                             background_color="white",
                             max_words=150,
                             min_font_size=5,
                             collocation_threshold=10).generate_from_text(s_text)

    o_word_cloud.to_file("words3.png")

    plt.figure(figsize=(10,8))
    plt.imshow(o_word_cloud)
    plt.axis("off")
    plt.tight_layout(pad=0)
    plt.show()
