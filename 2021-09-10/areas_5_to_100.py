from area_of_a_circle import area_circle

if __name__ == "__main__":
    for i_radius in range(5, 101, 5):
        f_area = area_circle(i_radius)
        print("Area of a circle of radius", i_radius, "is", f_area)
