import matplotlib.pyplot as plt

a_scores = [70, 20, 5, 3, 1, 1]
a_languages = ["Python", "Bash", "HTML", "jQuery", "Java", "PHP"]
a_colors = ["Red", "Blue", "Yellow", "Purple", "Green", "Cyan"]

plt.pie(a_scores, labels=a_languages, colors=a_colors)
plt.legend()
plt.show()
