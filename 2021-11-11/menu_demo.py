
class Menu:

    def print_menu(self):
        print("Menu")
        print("====")
        print()
        print("0. Exit")
        print()
        print("1. Show username")
        print()

    def get_min_and_max(self):
        return 0, 1


class Menu_Admin(Menu):
    def print_menu(self):
        super().print_menu()
        print("2. Print the file")
        print()

    def get_min_and_max(self):
        return 0, 2


def print_menu():
    print("Menu")
    print("====")
    print()
    print("1. print text.txt")
    print()
    print("0. Exit")


def ask_valid_number(i_min, i_max):

    b_exit = False

    # while True:
    while b_exit is False:
        s_number = input("Enter the number:")
        i_number = int(s_number)

        if i_number < i_min or i_number > i_max:
            print("Error! The number shoulber between", i_min, "and", i_max)
        else:
            print(s_number, " is valid!")
            #break
            b_exit = True

    return i_number

def read_file(s_file):

    s_file_contents = ""

    # If the file is not found an Exception will be raised
    with open(s_file) as file:
        for s_line in file:
            # print(s_line, end='')
            # print(s_line.rstrip())
            s_file_contents = s_file_contents + s_line

    i_length = len(s_file_contents)

    return s_file_contents, i_length


s_role = "USER"
o_menu = Menu()
s_username = input("Enter your username:")
if s_username == "admin":
    s_role = "ADMIN"
    o_menu = Menu_Admin()


while True:
    # print_menu()
    o_menu.print_menu()
    i_min, i_max = o_menu.get_min_and_max()
    i_selection = ask_valid_number(i_min, i_max)

    if i_selection == 0:
        print("Bye")
        exit()

    if i_selection == 1:
        print("Your username is:", s_username)

    if i_selection == 2:
        # Admin option only
        s_text, i_length = read_file("text.txt")
        print(s_text)
        print("The lenght of the text is:", i_length)
