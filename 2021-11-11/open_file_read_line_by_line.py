
if 1 + 1 == 2:
    print("Perfect!")

# exit()

s_filename = "text.txt"

# If the file is not found an Exception will be raised
with open(s_filename) as file:
    for s_line in file:
        # print(s_line, end='')
        print(s_line.rstrip())

