"""
def print_tree():
    print("  * ")
    print(" *** ")
    print("*****")
    print("  *  ")
    print("  *  ")
    print("")

i_counter = 5
while i_counter > 0:
    print_tree()
    i_counter = i_counter -1

"""

def print_tree(i_number=1, s_char="*"):
    while i_number > 0:
        print("    " + s_char)
        print("   " + s_char*3)
        print("  " + s_char*5)
        print(" " + s_char*7)
        print(s_char*9)
        print("    *  ")
        print("    *  ")
        print("")
        i_number = i_number - 1


print_tree(1, "*")
print_tree(1, "+")
print_tree(1, "#")
print_tree(4, ".")


