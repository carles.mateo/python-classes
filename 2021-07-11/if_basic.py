
b_true = True
b_false = False

if b_true is True:
    print("b_true is True")

if b_false is True:
    print("Will not enter here")
else:
    print("I told you it was false!")

if 7 == 7:
    print("This is true")

if 7 == 9:
    print("We will not enter, believe me.")

if not 7==9:
    print("Oh! tricky!")

s_name = input("What's your name: ")
if len(s_name) > 6:
    print("You name is long")
else:
    print("Your name is not long")

