
print("Welcome to the thirsty pub reservation system")
print("")
i_number = int(input("Enter a number of people:"))

if i_number < 3:
    print("Number of people acceptable")
elif i_number >= 3 and i_number <= 6:
    print("I've to check if there is room")
elif i_number > 6 and i_number < 11:
    print("I need to check with my manager. Call tomorrow")
else:
    print("Lads, there is a pandemic ongoing, wait a little for partying hard")
