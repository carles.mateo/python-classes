import random


def guess_my_number(i_num_secret, i_attempts):
    """
    This function asks the user to enter number and compares it
    :param i_num_secret: Number that computer thought
    :param i_attempts: attempts
    :return:
    """

    # Incrementem les vegades que has intentat endevinar-lo
    i_attempts = i_attempts + 1

    s_human_keyboard_entered = input("What is my number? :)  : ")

    # Converteix el que han entrat pel teclat a número integer
    i_human_keyboard_entered = int(s_human_keyboard_entered)

    if i_human_keyboard_entered > i_num_secret:
        print("My number is smaller than " + str(i_human_keyboard_entered))
        # Function calls itself in order to guess again
        guess_my_number(i_num_secret, i_attempts)

    if i_human_keyboard_entered < i_num_secret:
        print("My number is bigger than " + str(i_human_keyboard_entered))
        # Function calls itself in order to guess again
        guess_my_number(i_num_secret, i_attempts)

    if i_human_keyboard_entered == i_num_secret:
        print("Very well!!!! That's my number")
        print("It took: " + str(i_attempts) + " attempts.")
        if i_attempts > 5:
            print("This proves A.I. are much more clever than humans :)")


if __name__ == "__main__":

    i_random_number = random.randint(1, 100)

    print("I have thought a number between 1 and 100")
    print("Can you guess it? :)")

    guess_my_number(i_num_secret=i_random_number, i_attempts=0)
