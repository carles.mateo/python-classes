
if __name__ == "__main__":
    s_phrase = "It's Wednesday night and we are enjoying our Python class"

    s_phrase = s_phrase.lower()

    d_chars = {}
    for s_char in s_phrase:
        if s_char in d_chars:
            d_chars[s_char] = d_chars[s_char] + 1
        else:
            d_chars[s_char] = 1

    for s_char in d_chars:
        i_count = d_chars[s_char]
        print(s_char + " = " + str(i_count))
    