

if __name__ == "__main__":
    d_chars = { "a": "x",
                "b": "l",
                "c": "i",
                "d": "h",
                "e": "k"}

    s_phrase = "It's Wednesday night and we are enjoying our Python class"

    s_final_phrase = ""
    for s_char in s_phrase:
        if s_char in d_chars:
            s_final_phrase = s_final_phrase + d_chars[s_char]
        else:
            s_final_phrase = s_final_phrase + s_char

    print(s_final_phrase)

