import validation

if __name__ == "__main__":
    s_name = validation.ask_string("What's your name: ", 3, 10)
    s_city = validation.ask_string("What's your city: ", 5, 25)
    i_age = validation.ask_numbers("What's your age: ", 5, 120)

    print("Valid name: " + s_name)
    print(s_city)

