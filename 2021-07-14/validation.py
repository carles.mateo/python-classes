

def ask_string(s_text, i_min_length, i_max_length=100):
    while True:
        s_answer = input(s_text)
        if len(s_answer) < i_min_length or len(s_answer) > i_max_length:
            # Wrong
            print("Answer should be between " + str(i_min_length) + " and " + str(i_max_length) + " characters in length.")
        else:
            break

    return s_answer

def ask_numbers(s_text, i_min_accepted, i_max_accepted):
    while True:
        s_answer = input(s_text)
        i_answer = int(s_answer)

        if i_answer < i_min_accepted or i_answer > i_max_accepted:
            # Wrong
            print("Answer should be between " + str(i_min_accepted) + " and " + str(i_max_accepted) + ".")
        else:
            break

    return i_answer

