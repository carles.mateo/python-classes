# You will be able to find the source code in:
# https://gitlab.com/carles.mateo/python-classes

def display_enumerate():
    for i_key, s_name in enumerate(a_list_names):
        print(i_key, s_name)

def display_an_index():
    s_index = input("Give an index: ")
    i_index = int(s_index)

    if i_index >= len(a_list_names):
        print("Hey! you go too far, beyond the data...")
    else:
        print("Index " + s_index + " is " + a_list_names[i_index])

def display_all_data_jurassic():
    for i_key, s_name in enumerate(a_list_names):
        print(a_id[i_key], a_list_names[i_key], a_list_cities[i_key])


if __name__ == "__main__":
    # We define an Array
    a_list_names = ["Carles",
                    "Fabi",
                    "Kate",
                    "Pierre",
                    "Ganesh",
                    "Sandra",
                    "Albert",
                    "Edu"]

    # print(a_list_names[-3])
    # exit(0)

    # print(a_list_names[3:4])
    # print(a_list_names[::-1])
    # exit(0)

    # Arrays can contain same type or different types

    print(a_list_names)

    # display_enumerate()

    # display_an_index()

    a_list_cities = ["Cork", "Cork", "Cork", "Cork", "Cork", "Barcelona", "Barcelona", "Barcelona"]
    a_id = ["11111111A", "22222222B", "33333333C", "44444444D", "55555555E", "12345678F", "77777771G", "78901234C", "8888888D"]
    # display_all_data_jurassic()

    # exit(0)

    # Comment to the guys error in a_id

    a_data = [["11111111A", "Carles", "Cork"],
              ["22222222B", "Fabi", "Cork"],
              ["33333333C", "Kate", "Cork"],
              ["44444444D", "Pierre", "Cork"],
              ["55555555E", "Ganesh", "Cork"],
              ["12345678F", "Sandra", "Cork"],
              ["77777771G", "Albert", "Cork"],
              ["78901234C", "Edu", "Cork"],
             ]

    # for a_row in a_data:
    #     print(a_row[0], a_row[1], a_row[2])
    #    print("*"*40)

    for i_index, a_row in enumerate(a_data):
        s_line = ""
        for i_key, s_item in enumerate(a_row):
            # print(s_item)
            if i_key == 0:
                s_line = s_line + str(i_index) + ": "
            s_line = s_line + s_item + " "
        print(s_line)
        print("*"*40)

    i_index_w = 0
    while True:
        print(a_data[i_index_w][0] + " " + a_data[i_index_w][1] + " " + a_data[i_index_w][2])
        print("=" * 40)
        i_index_w = i_index_w + 1
        if i_index_w >= len(a_data):
            break

    i_index_w = 0
    while i_index_w < len(a_data):
        print(a_data[i_index_w][0] + " " + a_data[i_index_w][1] + " " + a_data[i_index_w][2])
        print("+" * 40)
        i_index_w = i_index_w + 1

    # @TODO: Show multilevel




    # Show dictionaries