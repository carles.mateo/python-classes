
def search(s_id):
    """
    Search by id
    :param s_id:
    :return: s_name, s_city (empty string if not found)
    """

    for a_row in a_data:
        if s_id == a_row[0]:
            print("Found")
            return a_row[1], a_row[2]

    return "", ""




if __name__ == "__main__":
    a_data = [["11111111A", "Carles", "Cork"],
              ["22222222B", "Fabi", "Cork"],
              ["33333333C", "Kate", "Cork"],
              ["44444444D", "Pierre", "Cork"],
              ["55555555E", "Ganesh", "Cork"],
              ["12345678F", "Sandra", "Cork"],
              ["77777771G", "Brian", "Cork"],
              ["78901234C", "Edu", "Cork"],
             ]

    s_id = "717777771G"
    s_name, s_city = search(s_id)
    if s_name != "":
        print(s_name, s_city)
    else:
        print("Error! Id " + s_id + " not found!")

